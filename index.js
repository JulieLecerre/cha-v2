import './index.scss';

import Tchat from './tchat';

const bots = [{
  name: 'bot',
  avatar: 'https://lh3.googleusercontent.com/proxy/hQm4Kupt4mNVLcAZWF1CO0x59m-X_OhfgadQ-0bL67B6O9-T1_XeaxGDrNTz2QRpNdDxE6iUaKSeka9tJ2VKnxNfatHPD0MF',
  actions: [{
    name: 'event',
    keywords: ['bonjour', 'hello'],
    event: () => 'hello'
  }, {
    name: 'botEvent',
    keywords: ['event', 'évènement'],
    event: () => {
      const url = 'https://opendata.paris.fr/api/records/1.0/search/?dataset=que-faire-a-paris-&q=&facet=date_start&facet=date_end&facet=tags&facet=address_name&facet=address_zipcode&facet=address_city&facet=pmr&facet=blind&facet=deaf&facet=transport&facet=price_type&facet=access_type&facet=updated_at&facet=programs';
      return fetch(url).then((res) => {
        console.log(res.json());
      });
    }
  }]
}, {
  name: 'totoBot',
  avatar: 'https://lh3.googleusercontent.com/proxy/hQm4Kupt4mNVLcAZWF1CO0x59m-X_OhfgadQ-0bL67B6O9-T1_XeaxGDrNTz2QRpNdDxE6iUaKSeka9tJ2VKnxNfatHPD0MF',
  actions: [{
    name: 'hello',
    keywords: ['bitcoin', 'bitcoins'],
    event: () => {
      const url = 'https://api.coingecko.com/api/v3/coins/bitcoin';

      return fetch(url).then((res) => {
        console.log(res.json());
      });
    }
  }]
}, {
  name: 'julie Bot',
  avatar: 'https://lh3.googleusercontent.com/proxy/hQm4Kupt4mNVLcAZWF1CO0x59m-X_OhfgadQ-0bL67B6O9-T1_XeaxGDrNTz2QRpNdDxE6iUaKSeka9tJ2VKnxNfatHPD0MF',
  actions: [{
    name: 'meteo',
    keywords: ['meteo', 'weather'],
    event: () => {
      const url = 'https://api.themoviedb.org/3/movie/tt10872600?api_key=2e604aa16414567598f221d546360a2f';

      return fetch(url).then((res) => {
        console.log(res.json());
      });
    }
  }]
}];

const tchat = new Tchat(bots);

tchat.run();
