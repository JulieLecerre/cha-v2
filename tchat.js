import Bot from './bot';

const Tchat = class {
  constructor(bots) {
    this.el = document.querySelector('#app');
    this.bots = this.createBots(bots);
  }

  renderBots() {
    return `
      <section id="bots" class="col-3">
        ${this.bots.map((bot) => this.renderBot(bot)).join('')}
      </section>
    `;
  }

  renderBot(bot) {
    const { status } = bot;
    const { name, avatar } = bot.data;
    const statusColor = status ? 'text-success' : 'text-danger';
    const statusName = status ? 'online' : 'offline';

    return `
      <div class="row mb-2">
        <div class="col-3">
          <img class="rounded-circle border border-primary img-fluid" src="${avatar}" class="img-fluid" alt="hero">
        </div>
        <div class="col-9 pt-1">
          <h5 class="h4">${name}</h5>
          <span class="h6 ${statusColor}">${statusName}</span>
        </div>
      </div>
    `;
  }

  renderMessages() {
    return `
      <section id="messages" class="row">
        <div class="col-12"></div>
      </section>
    `;
  }

  renderMessageSend(message) {
    const date = new Date();
    return `
      <div class="row mb-2">
        <div class="col-6"></div>
        <div class="col-6">
          <div class="card">
            <div class="card-header bg-dark text-light">
              ${date.getHours()}:${date.getMinutes()}
            </div>
            <div class="card-body">
              <h5 class="card-title">Cyril</h5>
              <p class="card-text">${message}</p>
            </div>
          </div>
        </div>
      </div>
    `;
  }

  renderMessageReceived(messageBot) {
    const { name, message } = messageBot;
    const date = new Date();

    return `
      <div class="row mb-2">
        <div class="col-6">
          <div class="card">
            <div class="card-header bg-dark text-light">
              ${date.getHours()}:${date.getMinutes()}
            </div>
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <p class="card-text">${message}</p>
            </div>
          </div>
        </div>
        <div class="col-6"></div>
      </div>
    `;
  }

  renderSendBare() {
    return `
      <form id="typing-message" class="row mt-2">
        <div class="col-10">
          <input type="text" class="form-control" placeholder="Send message">
        </div>
        <div class="col-2 d-grid gap-2">
          <button type="submit" class="btn btn-primary mb-3">Send</button>
        </div>
      </form>
    `;
  }

  render() {
    return `
      <header id="content" class="row">
        <nav class="navbar navbar-dark bg-dark">
          <div class="container-fluid">
            <span class="navbar-brand mb-0 h1">ChatBot</span>
          </div>
        </nav>
      </header>
      <main class="container-fluid">
        <div class="row mt-4">
          ${this.renderBots()}
          <div class="col-9">
            ${this.renderMessages()}
            ${this.renderSendBare()}
          </div>
        </div>
      </main>
    `;
  }

  createBots(bots) {
    return bots.map((bot) => new Bot(bot));
  }

  searchActionByBot(message) {
    const results = [];

    for (let i = 0; i < this.bots.length; i += 1) {
      const bot = this.bots[i];
      const messageAction = bot.findAction(message);

      if (messageAction.length) {
        const { name, avatar } = bot.data;

        results.push({
          name,
          avatar,
          message: messageAction
        });
      }
    }

    return results;
  }

  sendMessageBot() {
    const elMessages = document.querySelector('#messages > div');
    const elInput = document.querySelector('#typing-message input');

    const messagesBots = this.searchActionByBot(elInput.value);

    messagesBots.forEach((messageBot) => {
      console.log(messageBot);
      elMessages.innerHTML += this.renderMessageReceived(messageBot);
    });
  }

  sendMessage() {
    const elButton = document.querySelector('#typing-message button');
    const elInput = document.querySelector('#typing-message input');
    const elMessages = document.querySelector('#messages > div');

    elButton.addEventListener('click', (e) => {
      e.preventDefault();

      elMessages.innerHTML += this.renderMessageSend(elInput.value);

      this.sendMessageBot();

      elInput.value = '';
      elMessages.scrollTop = elMessages.scrollHeight;
    });
  }

  run() {
    this.el.innerHTML = this.render();

    this.sendMessage();
  }
};

export default Tchat;
