const Bot = class {
  constructor(data) {
    this.data = data;
    this.status = this.detectAction();
  }

  detectAction() {
    return !!this.data.actions.length;
  }

  findAction(message) {
    const { actions } = this.data;

    for (let i = 0; i < actions.length; i += 1) {
      const action = actions[i];

      for (let j = 0; j < action.keywords.length; j += 1) {
        const keyword = action.keywords[j];

        if (message === keyword) {
          return action.event();
        }
      }
    }

    return false;
  }
};

export default Bot;
